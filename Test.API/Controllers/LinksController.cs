﻿using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Test.API.Context;
using Test.API.Models;

namespace Test.API.Controllers;

[ApiController]
public sealed class LinksController : ControllerBase
{
    private readonly LinksContext _linksContext;

    public LinksController(LinksContext linksContext)
    {
        _linksContext = linksContext;
    }
    
    [HttpGet]
    [Route("{shortForm}")]
    public async Task<IActionResult> Get(string shortForm)
    {
        var result = await _linksContext.Links.SingleOrDefaultAsync(x => x.ShortForm.StartsWith(shortForm));
        if (result == null)
            return NotFound(new ErrorModel { Message = $"There is no link with such form: {shortForm}" });

        return Redirect(result.Source);
    }
    
    [HttpPost]
    [Route("/api/links/")]
    public async Task<IActionResult> Create([FromBody] CreateModel model)
    {
        await Task.Delay(70);
        using var sha = SHA1.Create();
        var linkBytes = Encoding.UTF8.GetBytes(model.Source);
        using var stream = new MemoryStream(linkBytes);
        var hashBytes = await sha.ComputeHashAsync(stream);
        var stringResult = new StringBuilder();
        foreach (var b in hashBytes)
            stringResult.Append(b.ToString("x"));

        var shortFrom = stringResult.ToString();
        var link = new Link
        {
            Source = model.Source,
            ShortForm = shortFrom
        };

        _linksContext.Links.Add(link);
        await _linksContext.SaveChangesAsync();

        var response = new CreateResponseModel
        {
            ShortForm = shortFrom[..10]
        };

        return Ok(response);
    }
}