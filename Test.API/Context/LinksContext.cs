﻿using Microsoft.EntityFrameworkCore;

namespace Test.API.Context;

public sealed class LinksContext : DbContext
{
    public LinksContext(DbContextOptions<LinksContext> options) : base(options)
    {
    }

    public DbSet<Link> Links { get; init; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Link>().HasKey(x => x.Id);
        modelBuilder.Entity<Link>().HasIndex(x => x.ShortForm).IsUnique();
        modelBuilder.Entity<Link>().Property(x => x.ShortForm).HasMaxLength(40).IsRequired();
        modelBuilder.Entity<Link>().Property(x => x.Source).HasMaxLength(512);
    }
}