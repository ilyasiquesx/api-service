using Microsoft.EntityFrameworkCore;
using Test.API.Context;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<LinksContext>(x =>
{
    x.UseNpgsql(builder.Configuration.GetConnectionString("Postgres"));
});

//builder.Services.AddHostedService<LinksMigrationBackgroundService>();

var app = builder.Build();

app.MapGet("/hc", async x =>
{
    await Task.Delay(80);
    await x.Response.WriteAsJsonAsync(new
    {
        Color = $"{Environment.GetEnvironmentVariable("ASPNETCORE_COLOR")} {Environment.MachineName}"
    });
});

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();
app.Run();