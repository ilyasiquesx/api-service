﻿namespace Test.API.Models;

public readonly record struct CreateResponseModel(string ShortForm);