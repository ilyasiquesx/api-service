﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Test.API.Migrations
{
    /// <inheritdoc />
    public partial class AddedUniqueIndex : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Links_ShortForm",
                table: "Links");

            migrationBuilder.CreateIndex(
                name: "IX_Links_ShortForm",
                table: "Links",
                column: "ShortForm",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Links_ShortForm",
                table: "Links");

            migrationBuilder.CreateIndex(
                name: "IX_Links_ShortForm",
                table: "Links",
                column: "ShortForm");
        }
    }
}
