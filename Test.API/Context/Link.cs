﻿namespace Test.API.Context;

public class Link
{
    public long Id { get; init; }

    public string Source { get; init; }

    public string ShortForm { get; init; }
}