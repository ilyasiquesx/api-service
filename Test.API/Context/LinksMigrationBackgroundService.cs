﻿using Microsoft.EntityFrameworkCore;

namespace Test.API.Context;

internal sealed class LinksMigrationBackgroundService : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;

    public LinksMigrationBackgroundService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await Task.Yield();

        using var scope = _serviceProvider.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<LinksContext>();
        
        if (context == null)
            return;

        if (!context.Database.IsRelational())
            return;

        await context.Database.MigrateAsync(cancellationToken: stoppingToken);
    }
}