﻿namespace Test.API.Models;

public readonly record  struct ErrorModel(string Message);